
#octagon
import turtle
myscreen = turtle.Screen()
myscreen.bgcolor('black')
myscreen.title('my pad')
myscreen.screensize(700,700)
pen = turtle.Turtle()
pen.speed(0)

for o in range(6):

    for colors in ('blue','red','yellow','green','orange','purple','pink'):
        for i in range(8):
            pen.color(colors)
            pen.forward(100)
            pen.left(45)
        pen.left(10)
turtle.hideturtle()
turtle.done()
