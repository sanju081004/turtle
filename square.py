#square
import turtle
myscreen = turtle.Screen()
myscreen.bgcolor('black')
myscreen.title('my pad')
myscreen.screensize(700,700)
pen = turtle.Turtle()
pen.speed(1)
for o in range(5):
    for colors in ('blue','red','yellow','green'):
        turtle.color(colors)
        turtle.right(20)
        turtle.forward(150)
        turtle.right(90)
        turtle.forward(150)
        turtle.right(90)
        turtle.forward(150)
        turtle.right(90)
        turtle.forward(150)
turtle.end_fill()

turtle.done()
