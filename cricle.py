#circle
import turtle
myscreen = turtle.Screen()
myscreen.bgcolor('black')
myscreen.title('my pad')
myscreen.screensize(700,700)
pen = turtle.Turtle()
pen.speed(0)

for o in range(6):

    for colors in ('blue','red','yellow','green','orange','purple','pink'):
          pen.color(colors)
          pen.circle(100)
          pen.left(10)

turtle.done()
