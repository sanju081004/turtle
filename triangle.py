#triangle
import turtle
myscreen = turtle.Screen()
myscreen.bgcolor('black')
myscreen.title('my pad')
myscreen.screensize(700,700)
pen = turtle.Turtle()
pen.speed(0)

for o in range(6):

    for colors in ('blue','red','yellow','green','orange','purple','pink'):
        for i in range(3):
            pen.color(colors)
            pen.forward(200)
            pen.left(120)
            
        pen.left(10)
turtle.done()
